package ru.diasoft.testTask.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.diasoft.testTask.detailsRequest.AddContactRequest;
import ru.diasoft.testTask.detailsResponse.AddContactResponse;
import ru.diasoft.testTask.services.contact.ContactServiceImpl;

@Endpoint
public class ContactController {
    private static final String NAMESPACE_URI = "http://localhost:8080/xml/contact";

    @Autowired
    private ContactServiceImpl contactServiceImpl;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "AddContactRequest")
    @ResponsePayload
    public AddContactResponse addContact(@RequestPayload AddContactRequest request) {

        return contactServiceImpl.addContact(request);
    }
}
