package ru.diasoft.testTask.models;

import lombok.*;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "contact_type")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactType", propOrder = {
        "id",
        "type"
})
public class ContactType{

    @Id
    @SequenceGenerator(name = "contactTypeSequence", sequenceName = "contact_type_generator_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contactTypeSequence")
    private Long id;

    @Column(nullable = false)
    @XmlElement(required = true)
    private String type;
}
