package ru.diasoft.testTask.models;

import lombok.*;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "contacts")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Contact", propOrder = {
        "id",
        "person",
        "contactType",
        "number"
})
public class Contacts {


    @Id
    @SequenceGenerator(name = "contactSequence", sequenceName = "contact_generator_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contactSequence")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_id")
    @XmlElement(required = true)
    private Person person;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contact_type_id")
    @XmlElement(required = true)
    private ContactType contactType;

    @Column(nullable = false, length = 20)
    @XmlElement(required = true)
    private String number;
}
