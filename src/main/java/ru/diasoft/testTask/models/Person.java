
package ru.diasoft.testTask.models;

import lombok.*;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "person")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Person", propOrder = {
        "id",
        "firstName",
        "lastName",
        "middleName",
        "position"
})
public class Person{

    @Id
    @SequenceGenerator(name = "personSequence", sequenceName = "person_generator_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "personSequence")
    private Long id;

    @Column(name = "first_name", length = 80,nullable = false)
    @XmlElement(required = true)
    private String firstName;

    @Column(name = "last_name", length = 80,nullable = false)
    @XmlElement(required = true)
    private String lastName;

    @Column(name = "middle_name", length = 80)
    @XmlElement(required = true)
    private String middleName;

    @Column
    @XmlElement(required = true)
    private String position;
}
