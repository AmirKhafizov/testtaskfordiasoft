package ru.diasoft.testTask.services.person;

import ru.diasoft.testTask.models.Person;

public interface PersonService {
    Person addPerson(Person person);
    void deletePerson(Long id);
    Person updatePerson(Person person);

}
