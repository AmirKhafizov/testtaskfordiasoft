package ru.diasoft.testTask.services.contact;

import ru.diasoft.testTask.detailsRequest.AddContactRequest;
import ru.diasoft.testTask.detailsResponse.AddContactResponse;

import java.util.List;

public interface ContactService {
    AddContactResponse addContact(AddContactRequest form);
    void deleteContact(Long id);
    AddContactResponse updateContact();
    AddContactResponse findContactById(Long id);
    List<AddContactResponse> findAllContacts();
}
