package ru.diasoft.testTask.services.contact;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.diasoft.testTask.dao.contactType.ContactTypeDao;
import ru.diasoft.testTask.dao.contacts.ContactsDao;
import ru.diasoft.testTask.dao.person.PersonDao;
import ru.diasoft.testTask.detailsRequest.AddContactRequest;
import ru.diasoft.testTask.detailsResponse.AddContactResponse;
import ru.diasoft.testTask.models.ContactType;
import ru.diasoft.testTask.models.Contacts;
import ru.diasoft.testTask.models.Person;

import java.util.List;

@Service
public class ContactServiceImpl implements ContactService {
    @Autowired
    private ContactsDao contactsDao;

    @Autowired
    private ContactTypeDao contactTypeDao;

    @Autowired
    private PersonDao personDao;

    public AddContactResponse addContact(AddContactRequest request){
        AddContactResponse addContactResponse = new AddContactResponse();
        Person person = Person.builder()
                .firstName(request.getFirstName())
                .lastName(request.getLastName())
                .middleName(request.getMiddleName())
                .position(request.getPosition())
                .build();

        ContactType contactType = ContactType.builder()
                .type(request.getType())
                .build();

        Contacts contact = Contacts.builder()
                .number(request.getNumber())
                .contactType(contactType)
                .person(person)
                .build();
        personDao.save(person);
        contactTypeDao.save(contactType);
        contactsDao.save(contact);
        addContactResponse.setContacts(contact);
        return addContactResponse;
    }

    @Override
    public void deleteContact(Long id) {

    }

    @Override
    public AddContactResponse updateContact() {
        return null;
    }

    @Override
    public AddContactResponse findContactById(Long id) {
        return null;
    }

    @Override
    public List<AddContactResponse> findAllContacts() {
        return null;
    }
}
