package ru.diasoft.testTask.detailsResponse;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.diasoft.testTask.models.ContactType;
import ru.diasoft.testTask.models.Contacts;
import ru.diasoft.testTask.models.Person;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "contacts",
        "person",
        "contactType"
})
@XmlRootElement(name = "AddContactResponse")
public class AddContactResponse {
    @XmlElement(required = true)
    protected Contacts contacts;

    @XmlElement(required = true)
    protected Person person;

    @XmlElement(required = true)
    protected ContactType contactType;
}
