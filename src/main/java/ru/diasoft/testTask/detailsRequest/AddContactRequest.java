package ru.diasoft.testTask.detailsRequest;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "number",
        "firstName",
        "lastName",
        "middleName",
        "position",
        "type"
})
@XmlRootElement(namespace = "http://localhost:8080/xml/contact", name = "AddContactRequest")
public class AddContactRequest {
    @XmlElement(namespace = "http://localhost:8080/xml/contact", required = true)
    protected String number;

    @XmlElement(namespace = "http://localhost:8080/xml/contact", required = true)
    protected String firstName;

    @XmlElement(namespace = "http://localhost:8080/xml/contact", required = true)
    protected String lastName;

    @XmlElement(namespace = "http://localhost:8080/xml/contact", required = true)
    protected String middleName;

    @XmlElement(namespace = "http://localhost:8080/xml/contact", required = true)
    protected String position;

    @XmlElement(namespace = "http://localhost:8080/xml/contact", required = true)
    protected String type;
}
