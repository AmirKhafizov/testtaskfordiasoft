package ru.diasoft.testTask.dao;

import java.util.List;

public interface CrudDao<T,I> {
    void save(T model);
    T find(I id);
    List<T> findAll();
    void update(T model);
    void delete(T model);
}
