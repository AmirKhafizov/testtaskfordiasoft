package ru.diasoft.testTask.dao.contacts;

import ru.diasoft.testTask.models.Contacts;
import ru.diasoft.testTask.dao.CrudDao;

public interface ContactsDao extends CrudDao<Contacts, Long> {

}
