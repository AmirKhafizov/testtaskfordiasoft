package ru.diasoft.testTask.dao.contacts;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.diasoft.testTask.models.Contacts;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ContactsDaoImpl implements ContactsDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void save(Contacts model) {
        entityManager.persist(model);
    }

    @Override
    public Contacts find(Long id) {
        return entityManager.find(Contacts.class, id);
    }

    @Override
    public List<Contacts> findAll() {
        return entityManager.createQuery("from Contacts contacts",Contacts.class).getResultList();
    }

    @Override
    @Transactional
    public void update(Contacts model) {
        entityManager.merge(model);
    }

    @Override
    @Transactional
    public void delete(Contacts model) {
        entityManager.remove(
                entityManager.contains(model) ? model : entityManager.merge(model));
    }
}
