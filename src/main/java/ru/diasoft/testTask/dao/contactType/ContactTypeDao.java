package ru.diasoft.testTask.dao.contactType;

import ru.diasoft.testTask.models.ContactType;
import ru.diasoft.testTask.dao.CrudDao;

public interface ContactTypeDao extends CrudDao<ContactType, Long> {
}
