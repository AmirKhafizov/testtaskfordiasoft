package ru.diasoft.testTask.dao.contactType;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.diasoft.testTask.models.ContactType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ContactTypeDaoImpl implements ContactTypeDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void save(ContactType model) {
        entityManager.persist(model);
    }

    @Override
    public ContactType find(Long id) {
        return entityManager.find(ContactType.class, id);
    }

    @Override
    public List<ContactType> findAll() {
        return entityManager.createQuery("from ContactType contactType",ContactType.class).getResultList();
    }

    @Override
    @Transactional
    public void update(ContactType model) {
        entityManager.merge(model);
    }

    @Override
    @Transactional
    public void delete(ContactType model) {
        entityManager.remove(
                entityManager.contains(model) ? model : entityManager.merge(model));
    }
}
