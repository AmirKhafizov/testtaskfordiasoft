package ru.diasoft.testTask.dao.person;

import ru.diasoft.testTask.models.Person;
import ru.diasoft.testTask.dao.CrudDao;

public interface PersonDao extends CrudDao<Person, Long> {
}
