package ru.diasoft.testTask.dao.person;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.diasoft.testTask.models.Person;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class PersonDaoImpl implements PersonDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void save(Person model) {
        entityManager.persist(model);
    }

    @Override
    public Person find(Long id) {
        return entityManager.find(Person.class, id);
    }

    @Override
    public List<Person> findAll() {
        return entityManager.createQuery("from Person person", Person.class).getResultList();
    }

    @Override
    @Transactional
    public void update(Person model) {
        entityManager.merge(model);
    }

    @Override
    @Transactional
    public void delete(Person model) {
        entityManager.remove(
                entityManager.contains(model) ? model : entityManager.merge(model));
    }
}
