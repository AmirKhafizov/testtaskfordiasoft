package ru.diasoft.testTask;

import ru.diasoft.testTask.detailsRequest.AddContactRequest;
import ru.diasoft.testTask.detailsResponse.AddContactResponse;
import ru.diasoft.testTask.models.Contacts;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {
    public ObjectFactory() {
    }
    public AddContactRequest createAddContactRequest(){return new AddContactRequest();}
    public AddContactResponse createAddContactResponse(){return new AddContactResponse();}
    public Contacts createContact(){return new Contacts();}
}
